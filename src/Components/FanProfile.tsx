import React, { FC } from 'react'
import {
  Typography,
  Grid,
  Button,
  CardContent,
  TextField,
  Link,
  IconButton,
} from '@material-ui/core'
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline'
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline'
import img from '../img.png'
       
const FanProfile: FC = () => {  
  return (
    <CardContent>
      <Typography variant="subtitle1" gutterBottom>
            Personal Data
      </Typography>
      <Grid container spacing={2}>
        <Grid item xs={8}>
          <form noValidate autoComplete="off">
            <Grid container spacing={2}>
              <Grid item xs={6}>
                <TextField
                  id="first_name"
                  label="First Name"
                  variant="outlined"
                  fullWidth
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  id="last_name"
                  label="Last Name"
                  variant="outlined"
                  fullWidth
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  id="country"
                  label="Country"
                  variant="outlined"
                  fullWidth
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  id="email"
                  label="Email"
                  variant="outlined"
                  fullWidth
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </Grid>
            </Grid>
          </form>
        </Grid>
        <Grid item xs={2}>
          <img
            src={img}
            alt="ChampUp cheer"
          />
        </Grid>
        <Grid item xs={2}>
          <Button variant="contained" color="secondary" size="small">
                Change Profile Picture
          </Button>
        </Grid> 
        <Grid item xs={12}>
          <Typography variant="subtitle1">Manage Profiles</Typography>
          <Typography variant="body2">
            <IconButton size="small" color="secondary">
              <AddCircleOutlineIcon fontSize="inherit" />
            </IconButton>
                Add new
          </Typography>
          <Typography variant="body2">
            <IconButton size="small" color="secondary">
              <RemoveCircleOutlineIcon fontSize="inherit" />
            </IconButton>
                Delete Profile
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Typography>
            <Link
              href="#"
              variant="body2"
              color="inherit"
              underline="always"
            >
                  Terms of Use
            </Link>
          </Typography>
          <Typography>
            <Link
              href="#"
              variant="body2"
              color="inherit"
              underline="always"
            >
                  Privacy Policy
            </Link>
          </Typography>
        </Grid>
      </Grid>
    </CardContent>
  )
}
export default FanProfile
