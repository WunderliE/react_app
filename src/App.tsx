import React, { FC } from 'react'
import { IntlProvider } from 'react-intl'
import { ThemeProvider, createMuiTheme } from '@material-ui/core'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import CssBaseline from '@material-ui/core/CssBaseline'
import { BrowserRouter as Router } from 'react-router-dom'
import Routes from './routes'
// import './App.css'

const App: FC = () => {
  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)')

  const theme = React.useMemo(
    () =>
      createMuiTheme({
        palette: {
          type: prefersDarkMode ? 'dark' : 'light',
          primary: {
            // light: will be calculated from palette.primary.main,
            main: '#363636',
            // dark: will be calculated from palette.primary.main,
            // contrastText: will be calculated to contrast with palette.primary.main
          },
          secondary: {
            main: '#1188aa',
          },
        },
        props: {
          MuiFilledInput: {
            margin: 'dense',
          },
          MuiFormControl: {
            margin: 'dense',
          },
        },
        overrides: {
          MuiButton: {
            root: {
              borderRadius: 0,
              textTransform: 'none',
            },
          },
          MuiTab: {
            root: {
              textTransform: 'none',
            },
          },
          MuiPaper: {
            rounded: {
              borderRadius: 0,
            },
          },
        },
      }),
    [prefersDarkMode],
  )

  return (
    <ThemeProvider theme={theme}>
      <IntlProvider locale="en">
        <>
          {/* place for ConfigCatProvider */}
          <Router>
            <CssBaseline />
            <Routes />
          </Router>
        </>
      </IntlProvider>
    </ThemeProvider>
  )
}

export default App
