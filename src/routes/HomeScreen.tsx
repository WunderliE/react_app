import React, { FC } from 'react'
import {
  Typography,
  Grid,
  Container,
  Divider,
  Box,
  Button,
  Card,
  CardContent,
} from '@material-ui/core'
import { Link as RouterLink } from 'react-router-dom'

const HomeScreen: FC = () => {
  return (
    <Container>
      <Card>
        <CardContent>
          <Box mb={3}>
            <Typography variant="h6" gutterBottom>
              Homepage - Judge
            </Typography>
          </Box>
          <Box mb={3}>
            <Typography component="p" gutterBottom>
            Your registration is currently under review. You will be notified when your status gets updated, and an email will be sent to your registered email address.  Until your registration is verified, you can only view but cannot edit pages. The tables will become editable when the Federation verifies your registration.
            </Typography>
          </Box>
          <Divider />
          <Box my={3}>
            <Typography gutterBottom>Please Select</Typography>
          </Box>
          <Grid
            container
            direction="row"
            justify="flex-start"
            alignItems="center"
          >
            <Grid item xs={2}>
              <Button
                variant="contained"
                color="secondary"
                component={RouterLink}
                to="/profile"
              >
                Shadow Judge
              </Button>
            </Grid>
            <Grid item xs={2}>
              <Button
                variant="contained"
                color="secondary"
                component={RouterLink}
                to="/profile"
              >
                Pro-Judge
              </Button>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </Container>
  )
}

export default HomeScreen
