import React, { FC } from 'react'
import {
  Tabs,
  Tab,
  Typography,
  Box,
  Container,
  Card,
} from '@material-ui/core'
import CoachProfile from "../Components/CoachProfile"
import FanProfile from "../Components/FanProfile"
import JudgeProfile from "../Components/JudgeProfile"

interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  )
}

function a11yProps(index: any) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  }
}

const ProfileScreen: FC = () => {
  const [value, setValue] = React.useState(0)

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue)
  }

  return (
    // sets left & right margin
    <Container>
      <Box mb={1}>
        <Card>
          <Box m={2}>
            <Typography variant="h6">Profile</Typography>
          </Box>
        </Card>
      </Box>
      <Box mb={3}>
        <Card>
          <Tabs value={value} onChange={handleChange} aria-label="profile tabs" variant="fullWidth">
            <Tab label="Judge" {...a11yProps(0)} />
            <Tab label="Coach" {...a11yProps(1)} />
            <Tab label="Fan" {...a11yProps(2)} />
          </Tabs>
          <TabPanel value={value} index={0}>
            <JudgeProfile />
          </TabPanel>
          <TabPanel value={value} index={1}>
            <CoachProfile />
          </TabPanel>
          <TabPanel value={value} index={2}>
            <FanProfile />
          </TabPanel>
        </Card>
      </Box>
    </Container>
  )
}

export default ProfileScreen
