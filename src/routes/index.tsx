import React, { FC } from "react";
import { AppBar, Toolbar, Tabs, Tab, Container } from "@material-ui/core";
import { Route, Switch } from "react-router-dom";
import HomeScreen from "./HomeScreen";
import ProfileScreen from "./ProfileScreen";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
    },
    content: {
      flexGrow: 1,
    },
    toolbar: theme.mixins.toolbar,
  })
);

const Routes: FC = () => {
  const classes = useStyles();

  return (
    <Switch>
       <Route exact path="/profile">
          <ProfileScreen />
      </Route>
      <Route path="/">
        <div className={classes.root}>
          <AppBar position="fixed">
            <Toolbar variant="dense" disableGutters>
              <Tabs value={1} aria-label="simple tabs example">
                <Tab label="Item One" value={1} />
                <Tab label="Item Two" value={2} />
                <Tab label="Item Three" value={3} />
              </Tabs>
            </Toolbar>
          </AppBar>
          <main className={classes.content}>
            <div className={classes.toolbar} />
            <Container>
              <HomeScreen />
            </Container>
          </main>
        </div>
      </Route>
    </Switch>
  );
};

export default Routes;
